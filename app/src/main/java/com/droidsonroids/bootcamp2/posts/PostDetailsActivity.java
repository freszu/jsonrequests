package com.droidsonroids.bootcamp2.posts;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.droidsonroids.bootcamp2.Constants;
import com.droidsonroids.bootcamp2.R;

public abstract class PostDetailsActivity extends BasePostActivity {

	protected EditText mPostTitleEditText;
	protected EditText mPostBodyEditText;
	protected LinearLayout mPostDetailsLinearLayout;
	protected String mPostId;

	@Override protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.post_details_linear_layout);
		findViews();
		handleSavedInstanceState(savedInstanceState);
	}

	protected void handleSavedInstanceState(final Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			mPostId = savedInstanceState.getString(Constants.KEY_POST_ID);
		}
	}

	private void findViews() {
		mPostDetailsLinearLayout = (LinearLayout) findViewById(R.id.post_details_linear_layout);
		mPostTitleEditText = (EditText) findViewById(R.id.post_title_edit_text);
		mPostBodyEditText = (EditText) findViewById(R.id.post_body_edit_text);
	}

	@Override protected void onSaveInstanceState(final Bundle outState) {
		outState.putString(Constants.KEY_POST_ID, mPostId);
		super.onSaveInstanceState(outState);
	}
}
