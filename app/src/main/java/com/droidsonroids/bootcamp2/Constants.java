package com.droidsonroids.bootcamp2;

public final class Constants {

	public static final int REQUEST_CODE_NEW_POST = 1;
	public static final int REQUEST_CODE_EDIT_POST = 2;
	public static final String EXTRA_POST = "EXTRA_POST";
	public static final String KEY_POST_ID = "KEY_POST_ID";


	
	private Constants() {
	}
}
