package com.droidsonroids.bootcamp2.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class Post implements Serializable{
	private String mObjectId;
	private String mTitle;
	private String mBody;

	public static Post fromJsonObject(final JSONObject jsonObject) throws JSONException {
		Post post = new Post();
		post.mObjectId = jsonObject.getString("objectId");
		post.mTitle = jsonObject.getString("title");
		post.mBody = jsonObject.getString("body");
		return post;
	}

	public String getTitle() {
		return mTitle;
	}

	public String getBody() {
		return mBody;
	}

	public String getObjectId() {
		return mObjectId;
	}
}
